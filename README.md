# Kubernetes Operator Lab

Team:

 * Ryan Jarvinen
 * Josh Wood
 * Jan Kleinert
 * Michael Hrivnak (replacing Marek Jelen)


Our latest lab content is available for review at:

>  [learn.openshift.com/operatorframework](https://learn.openshift.com/operatorframework/)

### Outline
Allotted time: 2 hours

 1. What is an Operator (controller and custom controller), and why do you care?
 2. See an Operator in action with Etcd
 3. Build the Memcached Operator with the Operator SDK (using Go)
 4. Build an Ansible-based Memcached Operator (contrast with Go one)
 5. The Operator Community (meet us in the booth for more interactive examples!)
